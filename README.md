<div id="badges" align="center">
<a href="https://www.linkedin.com/in/alecgogolev/">
  <img src="https://img.shields.io/badge/LinkedIn-blue?style=for-the-badge&logo=linkedin&logoColor=white" alt="LinkedIn Badge"/>
</a>
<a href="https://t.me/alecgogolev">
  <img src="https://img.shields.io/badge/Telegram-orange?style=for-the-badge&logo=telegram&logoColor=white" alt="Telegram Badge"/>
</a>
</div>
<div id="counter" align="center">
<a href="https://hits.seeyoufarm.com"><img src="https://hits.seeyoufarm.com/api/count/incr/badge.svg?url=https%3A%2F%2Fgitlab.com%2Fagogolev&count_bg=%232F62A4&title_bg=%23555555&icon=dot-net.svg&icon_color=%23FFFFFF&title=Profile+views&edge_flat=false"/></a>
<h1>
  Привет! Я Алексей Гоголев
  <img src="https://media.giphy.com/media/hvRJCLFzcasrR4ia7z/giphy.gif" width="30px"/>
</h1>
</div>
<h3> О себе </h3>
Я профессионал в области информационного обеспечения, выступаю в роли архитектора проекта, старшего разработчика и руководителя команд разработчиков информационных систем в том числе мирового уровня, имею глубокую экспертизу в поддержке разнообразных информационных продуктов, созданных на базе широкого спектра информационных технологий (Java, C#, C++, ASP.NET MVC, ASP.NET Core). В настоящий момент повышаю уровень своей экспертизы в области нейронных сетей и в частности нейросетей на базе генеративных сетей искусственного интеллекта типа ChatGPT и тому подобных.